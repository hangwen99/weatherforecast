const syncIntervalDuration = 10 * 1000; // seconds

const weatherAPI = "https://api.openweathermap.org/data/2.5/weather";
const apiKey = process.env.REACT_APP_WEATHER_API_KEY;

export { weatherAPI, syncIntervalDuration, apiKey };
