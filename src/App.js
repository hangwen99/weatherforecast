import axios from "axios";
import { useState } from "react";
import { toast } from "react-toastify";
import { Card, Flex, Input } from "antd";
import { DeleteOutlined, SyncOutlined } from "@ant-design/icons";
import { convertKelvinToCelcius, useInterval } from "./utils.js";
import "./App.css";
import Meta from "antd/es/card/Meta";
import {
  apiKey,
  syncIntervalDuration,
  weatherAPI,
} from "./constant.js";

const { Search } = Input;

function App() {
  const [cityName, setCityName] = useState();
  const [cityDetailListing, setCityDetailListing] = useState([]);

  const getForecastResult = (name) => {
    // call api to get data
    axios
      .get(`${weatherAPI}?q=${name}&appid=${apiKey}`)
      .then(({ data }) => {
        // add to array
        setCityDetailListing([...cityDetailListing, data]); // add detail to cityDetailListing
        setCityName(); // clear input
        toast("Successfully added new city to the list.");
        console.log(data);
      })
      .catch((error) => {
        const { code, message } = error;
        toast(`${code}: ${message}`);
      });
  };

  // remove item from list
  const removeFromList = (idToBeRemoved) => {
    setCityDetailListing(
      cityDetailListing.filter(({ id }) => id !== idToBeRemoved)
    );
  };

  const syncLatestData = (idx, name) => {
    // call api to get data
    axios
      .get(`${weatherAPI}?q=${name}&appid=${apiKey}`)
      .then(({ data }) => {
        // update array
        let latestList = cityDetailListing;
        latestList[idx] = data;

        setCityDetailListing(latestList); // add detail to cityDetailListing
        console.log(data);
      })
      .catch((error) => {
        const { code, message } = error;
        toast(`${code}: ${message}`);
      });
  };

  const renderWeatherCard = () => {
    return cityDetailListing.map(({ id, main, weather, name }, index) => {
      const { description, icon } = weather[0];

      return (
        <Card
          style={{ width: 180 }}
          hoverable
          actions={[
            <SyncOutlined
              onClick={() => {
                syncLatestData(index, name);
                toast("Synced successful with latest data.");
              }}
              key="remove"
            />,
            <DeleteOutlined onClick={() => removeFromList(id)} key="remove" />,
          ]}
          cover={
            <img
              alt="weather"
              src={`https://openweathermap.org/img/wn/${icon}@2x.png`}
            />
          }
        >
          <Meta
            title={name}
            description={
              <>
                <div style={{ textAlign: "right" }}>
                  {convertKelvinToCelcius(main.temp)}°C
                </div>
                <div style={{ textAlign: "right" }}>{weather[0].main}</div>
                <div style={{ textAlign: "right" }}>{description}</div>
              </>
            }
          />
        </Card>
      );
    });
  };

  useInterval(() => {
    // sync data every 10sec
    cityDetailListing.forEach((forecast, i) =>
      syncLatestData(i, forecast.name)
    );
  }, syncIntervalDuration);

  return (
    <div className="layoutWrapper">
      <div>
        <Flex gap={16} justify="center" wrap>
          {renderWeatherCard()}
        </Flex>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Search
          style={{ maxWidth: 700 }}
          autoFocus
          placeholder="Please enter a city"
          value={cityName}
          onChange={(e) => setCityName(e.target.value)}
          onSearch={(value) => getForecastResult(value)}
          enterButton="Search"
        />
      </div>
    </div>
  );
}

export default App;
