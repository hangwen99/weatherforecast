## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


## Weather Forecast web application

## Functionality
# Check weather forcast detail based on city name
1. Type the city name that you desired
2. Upon entering desired city name, hit "Enter" OR press the button "Search" to get the details.
3. You should see weather forecast detail. 

# Up to date synchronization
1. The listing will sync with latest data every 10 seconds.

# Manual synchronization 
1. A button made to immediately sync selected city with latest data.
2. Press the "Sync" icon button to ensure the data is up to date.

# Remove city
1. Press the right bottom button of the card, "Dustbin" icon, to remove a city from the listing.
